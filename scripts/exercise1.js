(function exercise1() {
  /**
   * Faisons des classes.
   * 
   * 1. Créer une fonction 'getDummyJson'
   *  a. retourner une promesse fetch()
   *  b. utiliser fetch() pour récupérer le fichier "./data/dummy.json"
   *  c. convertir la réponse en json
   *  d. toujours au sein de la fonction getDummyJson, ne retourner que la propriété data du JSON récupéré
   * 
   * 1. Créer une fonction 'getTextFile'
   *  a. utilser un paramètre url
   *  b. retourner une promesse fetch()
   *  c. utiliser fetch() pour récupérer le fichier passé en paramètre
   *  d. convertir la réponse en texte
   *  e. essayer de récupérer un fichier non existant sur le serveur "./data/nothing.json"
   *  f. sans test de la valeur de la réponse, la promesse fetch se résout sans erreur
   *  g. tester la valeur de la réponse et lancer une erreur si la réponse n'est pas ok
   *  h. attraper l'erreur et effectuer un console.warn disant que l'URL n'a pas pu être chargée
   * 
   */



  // TODO #1
  (async () => {
    console.info(`<Exercise 1. Step 1>`)
    try {
      const dummyJson = await getDummyJson()
      console.log(dummyJson)
    } catch (error) {
      console.warn('There a still errors in the step 1.')
      console.error(error)
    }
    console.info(`</Exercise 1. Step 1>\n\n\n`)
  })();


  // TODO #2
  (async () => {
    console.info(`<Exercise 1. Step 2>`)
    try {
      const data = await getTextFile('./data/nothing.json')
      console.log(data)
    } catch (error) {
      console.warn('There a still errors in the step 2.')
      console.error(error)
    }
    console.info(`</Exercise 1. Step 2>\n\n\n`)
  })();

})()